# Nisha Kolagotla Lab Notebook
#### Project: Deep Tunnel Mobilization

----

[[_TOC_]]

# 2/3/2022
Had a meeting with the ABE team members. Discussed the different ideas we have about the project. Decided to meet again on Sunday at 2pm on zoom.

We discussed our idea of using a motor to move a rail like a sliding gate. This is the idead that we submitted for our RFA. We realized that the motor might not have enough power, and therefore this idea might not be as feasible as we thought.

The next idea we had was to use pulleys to move the tunnel. We would use multiple pulleys which will be connected to a motor to move the tunnel.

The pulley idea also gave way to the idea to tow the tunnels using a truck. We decided that we need a way to make it more stable. The tracks would also have to be perfectly straight for this to work. We also came up with the idea of using johnson bars to manually move the tunnel. This can be used in conjunction with towing the tunnel.

<img src="Johnson_bar.jpg" width="500">

The last idea was to use cranes to lift the tunnel and move it to a new location.

# 2/6/2022
Met with all ABE and ECE team members.

We are leaning towards using the winch system.

## Block diagram
- Power system: Have the option of car battery. Better at extreme temperatures. BMS.
- Motor drive system (Look into which system is best).
- Motor: BLDC can be controlled. Need 4 motors.
- Sensors: Position to sense the motor. Maybe use a speed sensor. The sensor will be to make sure that both motors are in sync. Sensor to sense when the tunnel is going too far. Use LIDAR sensor. For rotary use either potentiometer or hall effect.
- Transmit signal to other motor.
- MCU: control the input to the motors and output of the sensors.
- Interface: Switch/LED to allow sensor to communicate with system.
- High voltage cable for power.
- Low voltage bus for sensor and MCU signals.
- Power down after a centain amount of time. Sleep after a centain amount of time.

## Sensors
- Position sensor
We are thinking of using a Lidar sensor. The LIDAR sensor to make sure the tunnel doesn't go too far in either direction.
Torque sensor to make sure one motor isn't pulling more than the other.

## Control Panel
- Power LED. If system is on or off.
- Fault LED
- Main power switch
- Emergency stop button
- Direction control switch
- LCD screen

Have a control panel on both sides. So we don't have to go back and forth.

# 2/8/2022
Talked to TA about our project.
- Showed our initial block diagram. Make text bigger. Add subsystems. Show which components are on PCB
- Try to be separate from the ABE team so we aren't reliant on them.
- Sent a text to Matt Turino to go down to the farm on Friday.
- Use off the shelf motor controllers since they will be high power.
- Weight of the tunnel might be too light to provide adequate traction to the wheels.

## Winch ideas
When we go to the farm look at the tracks. The wheels might not be able to move well (too much friction). Maybe look into using rubber wheels such as car tires.
 
We are concerned that one side of the tunnel might move faster than the other. We have some ideas on how to solve this.
- Use a laser and sensor array.
- Use one axel. Connect both sides of the tunnel to one axel.

# 2/15/2022
Talked to TA.
Decided to meet with the ABE team as soon as possible to get more information.

We are going to figure out what materials to buy if we were to make the system right now. After we meet with the ABE team, we have to decide if this project makes sense, and if we want to switch to a different project.

## Distance sensor
- Hall effect sensor
- Computer vision
- Accelerometer

## Concerns
1. Why do we need the ABE team? If we are just using a winch, we don't need any mechanical expertise.
2. If we only use one winch, how complicated will the project be? The only elecrical component would be the distance sensor.
3. What exactly is the problem we are solving? What have they tried so far? Why haven't those attempts suceeded?
4. We still haven't been able to go out to the farm. This is something that we would like to do to get information on the layout.
5. The ABE team seems to have decided on the gate idea that we first had. We need to get on the same page as them if possible.

We contacted the ABE team to hopefully meet on Thursday. Decide exactly what our role is and what their role is.

Before Thursday:
- Write down questions to ask Professor and ABE team.
- Pick out components: Winch, Hall effect sensor, magnet.
- Contribute to design document.

# 2/19/2022

There are simpler solutions that don't require ECE expertise. One solution is to use two cable winch pullers which are anchored at one end of the field. This would require only 2-3 people and cost around $100. However, if it should only require 1 person, we would need to use ECE expertise. This solution is to use ATV winches. We would need a battery management system to control the temperature and voltage of the battery. We would also need a motor controller circuit which controls when the contactor turns on or off.

ABE team is meeting with Dr. Grift on Monday to talk about the potential solutions. They don't want to make a decision until after that. We need to decide before that. They are planning to make a design decision by spring break which could be problematic for us. Also, cost is a problem. So, a solution that requires the ECE team might not be preferable.

We are going to contact Professor Witmer and Matt Turino to talk about our role in this project and potentially switching projects. We are also going to brainstorm more ideas to decide by Monday.

# 2/21/2022

### Met with ABE team and Dr. Grift.
Looked at pictures of the greenhouse.
There will be binding in the wheels. So the solution needs to solve this.
ABE ideas
- Gate mover
- Hand winch as a secondary system. Need Can use 12V battery as a power source.
Make a control system. Bluetooth? for us to have something to do.

Concern: left and right sides at same speed. Need to keep them at same speed. Dr. Grift recommends the winch solution. Specifically said to use one winch so the two sides are in sync.

Dr. Grift recommends implementing an app and using IoT/Bluetooth to control the motors. Solid state relay.
Recommends meeting with Matt. Ask what else needs to be automated since this is low tech. Use Matlab/Labview.

### Met with TA
Recapped meeting with Dr. Grift.
We are leaning towards using one winch.
Concerned about the practicality of adding bluetooth wih just one winch.
Concern is that the tunnels aren't fully automated. So, a human can be the failsafe to make sure that the tunnel doesn't go off the tracks.
Other ideas for the farm:
- Automate the plastic tarp to vent the system.
- Mount other sensors to monitor sunshine/temperature/weather.
Try to find other problems to solve.

The single winch is too simple. We should probably try to do the two winch idea.
Use the two winch idea for the design doc.
Not necessary to think about different projects right now.
IoT should be a seperate project. Maybe 3 mini projects.
Email Dr. Grift to potentialy meet earlier. We will try to come up with ideas and decide whether we can find other ideas on the farm.

# 2/22/2022

Met with Prof Fliflet and Evan. Went to the farm. For design doc just assume design with one winch.
We have started working on the design doc.
There is not much place on either side of the tunnel.
There are probably attachment points on the tunnel.

# 3/2/2022

Met with Evan to discuss the design document and review.

## What to include in design document

- How winches work.
- How ultrasonic sensor mounted.
- How winch mounted.
- Decide one design for design doc.
- Look up winch spec.
- Don't need separate motor controller
- Use relay not transistor.
- Make block diagram easier to read.

For the presentation show what is inside of the winch. How will we modify it?

# 3/7/2022

In the interim, we found out from Matt that there is a tractor we can use to anchor the winch.

Met with Dr. Grift for a project update.

## ABE update

Ordered parts.
Use a truss that is light enough. Use a sled so the truss can slide on the ground. The problem is there is not much space at the end of the tunnel.
The electrical winch is beeter because there is more control.
Pul from the front and the back so all the force is not from the front of the tunnel.

## ECE update

Went over the control system for the winch. We are planning to attach a switch to the solenoid box.
We won't need as big a relay.
Add overload protection. AutomationDirect makes a heavy duty relay.
Dr. Grift recommends that we use the Arduino starter kit. We can't do so and are planning to use a STM32 microcontroller.

# 3/8/2022

Met with TA.
We are sticking to the design document design.
Went over MCU pcb schematic.
- Connect battery and hard ground.
- Use panel mount switch.
- Current seinsor which is a coil wrapped around wire.
- Include programming header on schematic.

Meet more in person. Better team communication. Outline objectives more clearly.

Downloaded Microchip studio to start programming the microcontroller.

## Enclosures

All the enclosures we are lloking at are from polycase.
Screws for all ML and HD enclosures are MBR-100. So we should use M3 mounting holes.
Chose the HD-22F enclosure. This enclosure is waterproof.

<img src="hd-22fmmt_iso_1.jpg" width="500">

### Mounting hole dimensions

All use M3 screws except AN-x series.
- HD-24FMMT: 1.063"x3.063" = 27mm x 77.8mm
- HD-44F: 2.563"x2.563" = 65.1mm x 65.1mm
- HD-22F: 1.125"x1.125" = 28.575mm x 28.575mm
- ML-44F: 2.250"x3.438" = 57.15mm x 87.3252mm
- ML-34F: 1.969"x2.375" = 50.0126mm x 60.325mm and 1.125"x3.344" = 28.575mm x 84.9376mm
- ML-45F: 3.250"x3.063" = 82.55mm x 77.8mm and 1.875"x4.438" = 47.625mm x 112.7252mm
- ML-46F: 1.781"x1.719" = 45.2374mm x 43.6626mm and 2.469"x1.094" = 62.7126mm x 27.7876mm
- ML-47F: 2.524"x1.714" = 64.1096mm x 43.5356mm and 3.211"x1.089" = 81.5594mm x 27.6606mm
- HD-45F: 2"x1.75" = 50.8mm x 44.45mm and 2.75"x1.063" = 69.85mm x 27mm
- AN-04F: 3.03"x2.825" = 76.926mm x 71.755mm
	- Uses M3.5 screw
- WP-36: 3.858"x2.205" = 97.9932mm x 56.007mm
- AN-09F: 3.5"x2.717" = 88.9mm x 69.0118mm
	- Uses M3.5 screw
- SG-19: 1.575"x3.937" = 40mm x 99.9998mm and 3.858"x3.287" = 97.9932mm x 83.4898mm
- AN-05F: 4.331"x3.543" = 110.0074mm x 89.9922mm
	- Uses M3.5 screw
- WQ-46: 4.594"x4.594" = 116.6876mm x 116.6876mm
- WQ-55: 4.73"x5.919" = 120.142mm x 150.3426

## Microchip studio code

I looked at the example code for turning on an LED. It uses c and is similar to android studio. This will hoefully make it easier to program.
There is also example code for the LCD display and sensors. We should be able to repurpose some of this code. The LCD that we are using is different from the example that is given, but it is still helpful.

### LCD code
This is an interesting article <https://circuitdigest.com/microcontroller-projects/lcd-interfacing-with-8051-microcontroller-89s52>.
The example on the Adafruit website uses Arduino, but any microcontroller should work.  
Information about commands for LCD <https://learn.adafruit.com/usb-plus-serial-backpack/command-reference>:
- 0xFE is the command start character.
- Display on
	- 0xFE 0x42
		- Turns display backlight on
	- 0xFE 0x46
		- Turns display backlight off
- Set brightness
	- 0xFE 0x99
	- 0xFE 0x98
		- Set and save brightness
- Set contrast
	- 0xFE 0x50
	- 0xFE 0x91
		- Set and save contrast
- Austoscroll on
	- 0xFE 0x52
- Austoscroll off
	- 0xFE 0x52
- Clear screen
	- 0xFE 0x58
- Change startup splash screen
	- 0xFE 0x40
- Set cursor position
	- 0xFE 0x47
- Go home
	- 0xFE 0x48
- Cursor back
	- 0xFE 0x4C
- Cursor forward
	- 0xFE 0x4D
- Underline cursor on
	- 0xFE 0x4A
- Underline cursor of
	- 0xFE 0x4B
- Block cursor on
	- 0xFE 0x53
- Block cursor off
	- 0xFE 0x54
- Set RGB backlight color
	- 0xFE 0xD0
		- Red: 0xFE 0xD0 0xFF 0x0 0x0
		- Blue: 0xFE 0xD0 0x0 0x0 0xFF
		- White: 0xFE 0xD0 0xFF 0xFF 0xFF
- Set LCD size
	- 0xFE 0xD1
- Create custom character
	- 0xFE 0x4E
- Save custom character to EEPROM bank
	- 0xFE 0xC1
- Load custom character rom EEPROM bank
	- 0xFE 0xC0

## Other links
- [Microcontroller](https://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48P_88P_168P-DS40002065A.pdf)
- [LCD Adafruit](https://www.adafruit.com/product/782#description)
- [LCD information](https://circuitdigest.com/microcontroller-projects/lcd-interfacing-with-8051-microcontroller-89s52)
- [AVR programming](http://avrprojectsfiuady.blogspot.com/2011/07/programming-avr-with-avrisp-mkii.html)
- [Polycase](https://www.polycase.com/hd-22f)

# 3/10/2022

## Questions for ABE team
- Do you need to use the machine shop? We need to go over expectations with regards to the machine shop.
- What equipment do you have access to in the tractor lab?

## TODO
- Order parts
- PCB redesign
- Talk to ABE team
- Revise design document
- Assign tasks to each other

### Specific tasks
We will meet next Friday during Spring Break. If we can finalize parts by the end of Spring Break, we can order them by 3/21.
- Jonathan
	- Finish PCB design
- Nisha
	- Code for microcontroller
	- State machine/logic diagram
	- Code for LCD. What to display based on state.
- Youssef
	- Make a circuit diagram of relay and switches for the design document so that other people who are reading the design document will understand what is going on. Original circuit and how we are modifying the circuit for our project.

# 3/16/2022

Youssef is planning to come back to campus in April. So, we might switch roles so he is in charge of the programming.

# 3/22/2022

We are going to the machine shop with the ABE team tomorrow at 11AM. 

Met with Evan. We have two pcbs(power, and microcontroller) to pick up. I will pick them up today.
Someone needs to focus on the ground anchoring part.

Design document:
- It might not be well anchored to the ground.
- A tractor battery would be better since it wouldn't drain as fast.

Timeline for mechanical components: we will hopefully figure this out Friday/Monday.

CFOP number for ordering parts: 1-626749-933011-191000.

I picked up the pcbs. There are five copies of each pcb, so we can experiment with the components.

# 3/23/2022

Met with the machine shop with the ABE team.
We only have four more weeks to work on the project. So, we need to figure all of this out as soon as possible.

- The ABE team has a drawing of their idea for pulling the tunnel.
	- They want to pull the tunnel rom both the back and front so that it doesn't collapse.
- Concerns about the ABE design
	- There will be a lot of rop needed to pull the tunnel.
	- We still don't know how to attach the rope to the tunnel supports. The horizontal bar might be a better anchor since it is more sturdy.
	- There are multiple ways to attach the rope. Welding, clamps, drill holes.
- We still don't have the winch. Prof. Witmer has said that there is a funding problem and won't approve the winch. So, we wrote to Prof. Fliflet about what funding is available and are going to get the winch with the idea of returning it later.
- We need the winch and the dimensions of the tunnel and pipes before talking to the machine shop again. So, we are going to schedule a meeting with Matt to ask about specifics.
- There is a lot we can work on without the ABE team. Since we got the PCBs, we will start soldering. Also, we are going to try to figure out the winch connector.

TODO:
- Acquire parts.
- Get dimensions from Matt.
- Figure out a concrete design.
- Talk to the machine shop.
- Solder pcbs.
- Finish microcontroller code.

# 3/26/2022

## State Machine

This is a general state machine for the operation of the system.

States:
- Off: system is turned off.
- Power on: switch is pressed to turn on the battery.
- Winch on: switch is pressed to turn on the winch.
- LCD1: LCD display indicates winch is on.
- Ultrasonic1: check ultrasonic sensor. If tunnel not close, loop back.
- Ultrasonic2: tunnel is approaching the sensor.
- LCD 2: LCD display indicates tunnel is approaching ultrasonic sensor.
- Winch off: winch shuts off automatically once tunnel reaches ultrasonic sensor.
- Power off: switch is pressed to turn off battery.
- Off: system is turned off.

State machine:

<img src= "state_machine.jpg" width="500">

## Testing microcontroller

Blink LED. I am using this to make sure the IO pins and LED work.
Once I am sure the pins work I am going to test the relay using the osciloscope.
This testing is being done on a breadboard before testing the soldered pcb. The programmer is connected to the breadboard with woldered wires.

```c
#include <asf.h>
#include <avr/io.h>
#include <util/delay.h>
int main (void)
{
	LED_Off(LED0);
	_delay_ms(1000);
	LED_On(LED0);
	_delay_ms(1000):
}
```
Microchip studio recognizes the board correctly. But doesn't see the programmer. I tried Arduino studio as well, but I can't find this specfic board and it didn't program correctly with the board I picked. We think it's something wrong with the programmer since it recognizes the board.

# 3/28/2022

We tried programming the microcontroller using one of the programmers from the 445 lab. It didn't work, so this means the problem wasn't our programmer. We then tried using a different computer to program the board, but that had the same problem.

Since we weren't able to use the AVR ISP to program the microcontroller, we are trying to program it using an Arduino Nano. <https://docs.arduino.cc/built-in-examples/arduino-isp/ArduinoToBreadboard>.

The bootloader instructions from the Arduino website didn't work. So, we are now following the directions at <https://www.brennantymrak.com/articles/programming-avr-with-arduino.html>. This tutorial isn't working as well. We tried switching the microcontroller, Arduino, and all the components. 

# 3/29/2022

The ABE team sent an email to Matt saying that they are planning to build a 1/3 scale prototype due to funding concerns. Matt has since offered to help pay for the winch to be able to build a working system. Either way, it shouldn't affect us much. We are going to meet with Matt on Thursday and will hopefully be able to resolve the funding issue.

Met with TA:
We were able to get the programmer to work on one of the lab computers. So, we have a solution for now.
PIN_PC4

# 3/30/2022

## Ultrasonic Sensor Psuedocode

```
// Set trig pin low
// delay 10us
// Set trig pin high
// delay 10us
// Set trig pin low
// Read echo pin
// calculate distance
```

# 3/31/2022

Met with Matt.
There is a siginificant price difference of the scale model to the full size winch. It is a ~$150 difference. The cables are also a large expense. The ABE team is planning to use a hand winch to implement a working solution as well.
The tractor is at the Fruit Research Building (2711 S. Race St.). We are going to meet at 11AM on Tuesday.
The ABE team has access to the Tractor lab. They are going to start working on the prototype today.
The [winch](https://www.harborfreight.com/automotive/winches/1500-lb-capacity-120v-ac-electric-winch-61672.html) was ordered in from Harbor Freight. It is rated for 1500lbs.

# 4/5/2022

Met with the TA. We were able to get the microcontroller programmed on Evan's computer and on the lab computer. However it did not work on my computer.
We decided to use the original pcb for now. We are going to cut the traces and solder additional wires to rewire the microcontroller.
For the ultrasonic sensor, we are going to attach it to a stake which will be attached to the end of the tracks. We are going to talk to the machine shop about this.
We were able to get the LCD panel working. The problem was the drivers, and we had to install many different drivers to make it work. It works on the pcb as well.

# 4/7/2022

We got the winch from Harbor Freight with the intention of later returning it. Went to the machine shop to talk about the winch. Gave the dimensions of the truck hitch. We decided to attach the winch to a metal plate which can be attached to the truck. The machine shop is also going to buy a winch so we can return the other one. The machine shop will also help us drill holes in the pcb if we can get the measurements in by Monday.
Youssef also had an idea to attach the cables to a stable point above the door of the tunnel. He agreed but said that we shouldn't worry about attaching the winch to the tunnel.

# 4/8/2022

Trying to test the ultrasonic sensor. I followed an online tutorial to test if the ultrasonic sensor works. However, I can't open the serial monitor. The error message says "Board at COM4 is not available". So, I used a LED to test the sensor. It did work. The LED turned on when I put my hand in front of the sensor. So, at least we have proof of concept.
The jumper cable we are using to connect the LCD screen isn't working correctly. The wires are disconnecting from the header. I tried soldering the wires, but the solder made the wires too big.

# 4/13/2022

We gave the polycase and sensors to the machine shop. I am planning to pick it up later today. We sent an update to the ABE team and hope to meet tomorrow at the tractor lab.

The microcontroller code is mostly done. There are some decision that need to be made on the system functionality. I still need to test the ultrasonic sensor. My last tests show that the sensor works, but not very accurately. The LCD jumper cable is still not fixed, so that is a priority as well. We picked up the new pcbs yesterday, so we would be able to test with that soon.

TODO:
- Microcontroller code
	- Panel LED
	- Ultrasonic sensor
	- LCD functionality
	- Halleffect sensor
- Flowchart/state machine update
- Test ultrasonic sensor
- Test with ABE team
- Mock demo preperation

Links:
- [Current sensor datasheet](https://www.tamuracorp.com/electronics/en/pdf/202104/L37SxxxS05_e.pdf)
- [Ultrasonic sensor code](https://howtomechatronics.com/tutorials/arduino/ultrasonic-sensor-hc-sr04/)
- [LCD pinout](https://cdn-shop.adafruit.com/datasheets/rgblcddimensions.gif)

We were able to test the LCD and ultrasonic sensor. The output of the ultrasonic sensor correctly displays on the LCD screen. Now, we are testing the current sensor.

We connected the current sensor to the microcontroller on a breadboard. Then, we used the current sensor to measure the current through an LED. The current wasn't high enough to register on the microcontroller input. Then we tried testing the current sensor through one of leads on the winch, This current still isn't high enough.

# 4/16/2022

We are going to meet with the ABE team on Tuesday(4/19) at 9AM to hopefully test the completed system.
We connected the switch to the winch, and can now use the switch to control the winch.

# 4/17/2022

We tested the new current sensor. This one works with a lower current. We are going to calibrate it on the AbE winch on Tuesday.
We also tested the relay on the breadboard and confirmed that circuit works.
The jumper cable for the lcd is broken, so we ordered more which are due to arrive on Tuesday.

# 4/18/2022

We added a timer to control the relay as well. The duty cycle is 15 min long, with 45 sec high then 14:15 low. This worked as expected, when we tested for a cycle of 90 sec with 4.5 sec high.

The current sensor isn't working anymore. We tested it with the VDD and GND switched by mistake and the sensor got really hot. When connected to the lcd, it always reads around 209. The current sensor also came with a seven segment display which should display the current. So, we tested it with that and it showed FFF. So, we are concluding that it is a problem with the sensor itself and are planning to order a new one.

I added a clock to the lcd screen. I shows the time elapsed since the program started running. After 1 day it will just show ">1day". 

The relay circuit and switch works on the breadboard. We are testing it on the pcb. The connecter to power the pcb has a loose connection. After fixing the connection, the relay now works. The ultrasonic sensor works as well. It stops the winch when there is an obstacle <20cm from the sensor. 

# 4/19/2022

We net with the ABE team in their tractor lab. They have a 1/3 scale truss built. The power supply that we brought isn't enough to supply our winch. So, we are going to use car jumper cables to power the winch. However, we can't test our switch since we don't have a working current sensor.
We tested the winch with the ABE team's truss, and the cables got tangled in the winch. We brought the winch back to the machine shop and they were able to untangle it.

# 4/22/2022

We got the new current sensor and tested it. It works this time. The problem we were having before the other sensor broke was that the sensor was connected to a digital pin instead of an analog pin.

# 4/23/2022

We tested the current sensor on the pcb with the switch. The analogRead output is showing 705 even when the current goes up to 12A.

We cut an acrylic sheet to a stake and attached the ultrasonic sensor to it.

We were able to get the system to work and mounted the polycase and solenoid box on the metal plate with copper wire.

# 4/24/2022

We replaced the copper wire with double sided tape so the metal plate looks cleaner.

# 4/30/2022

The ultrasonic sensor is mounted to an acrylic stake which will be placed 20 cm from the end of the tunnel. When the tunnel reaches the end of the track, the ultrasonic sensor will signal to the relay to cut power to the winch.
The Hall effect sensor is used to moniter the current. If the cable of the winch gets caught on something, the current input will increase. The sensor will then send a signal to the relay to cut power to the winch.

