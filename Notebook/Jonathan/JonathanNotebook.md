# Jonathan's NoteBook

[[_TOC_]]

## 1/28/2022 Initial meeting

Worked as a team to write an RFA and discuss some intial thoughts about the project. Dicussed some possiblities of solutions to the Deep Tunnel Mobilization project and the different components that would be involved in the project.

## 2/1/2022 Setup Repo

Short Meeting to setup of the Repo on GitLab so share files and start the Markdown files for notebooks

## 2/3/2022 First full team meeting with ABE 469

Discussion on how to power the moving of the tunnel. In the past they used a truck to help move the high tunnel. The main issue with this approach was the constant need to readjust the lines that are attached to the high tunnel. The lines would pull one side a bit more than the other side.

The goal with the new system is to adjust the lines so that they are parallel with the direction of movement.

### Some of our solutions that were proposed

1. Winch System

    Much like many cars have a winch at the front of the car to get out of tough situations, winches mounted at each end of the high tunnel that can pull the high tunnel across the length of the field when ever necessary.

    Some things to consider if we use this system:
    * A mounting system will need to be constructed to ensure that the system does not pull itself out of the ground.
    * A control system needs to be developed to make sure that both of the winches on each side pull in syn with each other and that the other winches on the other side are able to unwind at the same rate.
    * A nice feature maybe to have a system to easily remove the cables when the high tunnel does not need to be moved

2. Distributed Direct Drive

    In this system motors will be spread out across the high tunnel to provide an even amount of power across the whole structure just as if there were 10 people pushing the structure from each of those points.

    Some issues to consider with this system:
    * A complex control system will need to be developed to ensure that each motor is turned the appropriate amount.
    * potential to offer redundancy due to the use of many motors.

3. Sliding Gate
    The inspiration of this idea comes from the sliding driveway gates on expensive homes and the sliding gate that was at my high school. In these systems a motor in housing drives a gear system that either directly drives some tracks or a belt that moves the gate. For our application. we would scale this system up with multiple motors to drive the whole system.

    Some issues to consider with this system:
    * The belt method might not be practical with the lenght of the high tunnel.
    * the second major concern would be the motors on the system if the would have to be physically picked up and moved every so offen so that the high tunnel could be powered.

4. [Johnson Bar](https://www.saferack.com/wp-content/uploads/2016/09/capstans-railcar-movers-740x516.jpg)

    The goal with this system is to have a manually back way to push the structure without needing to use as many people to move the structure in case of a power failure. This is based on the idea for moving rail cars by hand based on torque.

    Some issues to consider with this system:
    * I don't think that this will fulfill the ECE requirments for a Senior Design project.
    * used to push rail cars where there is a lower amount of friction between the car and rails verses the wheels and the track that is currently in place.

    ![Image](https://www.saferack.com/wp-content/uploads/2016/09/capstans-railcar-movers-740x516.jpg)

5. Youssef's Crane

    The crane system would pick up the structure so that it is easier to move. The idea is that the structure has a lot of wieght so it would have a large frictional force to move it. If there was less frictional force, then it would be eaier to move.

    Some issues to consider with this system:
    * Building a crane might be impractical and out of the scope of this project considering the amount of time and resources that are available.

### General overall considerations to take into account

#### Power source

The two main power sources for this project will either be a battery system or wall outlet. Each will need their own set of power electronics to delivery the amount of power necessary to each of the systems that are involved in the project.

Some things to consider:

* The type of battery system: 12V or 24V car battery.

* A custom made rechargeable battery pack.

* Direct power from a 110 or 120 AC wall plug at 60 Hertz.

Either way a control system will need to convert from one type of power source to another.

## 2/6/2022 Meeting with ABE 469

Mainly discussed the project proposal and decided on using a winch system to pull the high tunnel across the field instead of the other ideas that were brought forward. The idea still needs some of the other details to be defined like the method for winching the tunnel across the field and the alignment system to keep everything in place. Started work on the Proposal to define what components would be needed to move the high tunnel.

### Components

* Planning for four motors at each corner of the high tunnel to pull in each direction.
* The battery pack is the main power source in the system.
* There is a system of high voltage and low voltage cables across the length of the field to transfer power for the motors, sensors, and relay data information.
* We need a system to check the alignment of the high tunnel as it moves across the field. This will need to feedback to the microcontroller to ensure that the tunnel moves across the field without a problem.
* The use of LIDAR sensors for the distance to the end of the field.
* Possible sleep function for the system to power down when not in use.

## 2/8/2022 Meeting with TA

Discuss the feasability of the design and some of the concerns with the project so far. Also talking with David Null about the control issues of the aligning the structure as it travels along the field.

### Key Meeting info

* Next things is to scope more of the site. We want to have a better idea of the size and scope of the object that we are moving as soon as possible.
* The text in the block diagram need to be bigger.
* If possible, we want to own as much of the project as possible apart from the ABE folks in case they are not able to deliver on any of their goals. This way we would have something to show for at the end of the semester.
* For the different components that we choose, many of them will be off the shelf since there is no need to reinvent a motor controller board. However, there is still a lot of parts to design.
* The goal is to make the design as fool proof as possible so that it is easy and intuitive for the operator to use the system.
* The major problem right now is the alignment sensors and how to implement that. We need to do more research into seeing what other systems have done something similar and how that can be implemented in the high tunnel.

## 2/10/2022 ECE Team meeting

Our main objective for the meeting was to finish the project [proposal](https://courses.engr.illinois.edu/ece445/getfile.asp?id=19809).

While working on the proposal, we came across some interesting information related to high tunnels. One of the interesting examples that has been done in the past to move high tunnels is explained in [this article](https://www.uky.edu/ccd/sites/www.uky.edu.ccd/files/MovableHighTunnels-CCD-SP-15.pdf). Although they do not automate the moving process as shown [here](https://www.youtube.com/watch?v=-GZ_p5cP96E), there is a lot of thought that goes into the construction and placement of the high tunnels. Some of the techniques used by their team are deal-breakers for the current team at Student Sustainable Farms.
Many examples of moving high tunnels that I have found including [Shared Legacy Farms](https://www.youtube.com/watch?v=Ha9VqUIUZ08) and [Neversink Farms](https://www.youtube.com/watch?v=R-nKve2TCR8) move the high tunnels by hand on tracks. However, in these cases a minimum of four people are needed to move the high tunnel rather the one or two people that the Student Sustainable farm is aiming for.

Many other Universities and initiatives have been made to support the use of high tunnels like [Washington State University](https://plasticulture.wsu.edu/high-tunnelshoophouses/), [Penn State Extension](https://extension.psu.edu/high-tunnel-tomato-production-basics),[Natural Resources Conservation Service](https://www.nrcs.usda.gov/wps/portal/nrcs/detailfull/national/programs/?cid=stelprdb1046250), and [Rutgers University](https://sustainable-farming.rutgers.edu/wp-content/uploads/2014/09/High-Tunnel-Moveable-RunnerRail-Assembly-SARE-FW07-319.pdf), but they have not made the system moveable or it is only moveable by hand.

### Satellite view

From the [Satellite view](https://www.google.com/maps/place/Sustainable+Student+Farm/@40.0797672,-88.217522,129m/data=!3m1!1e3!4m5!3m4!1s0x880cd6f002562e19:0x5ffbd796430d2fef!8m2!3d40.079908!4d-88.2167776) of the [Student Sustainable Farm](https://www.thefarm.illinois.edu/), we can get a picture of why two tractors cannot move the high tunnel like in the case of the University of Kentucky. There is simply no room on either side to pull all of the high tunnels the required distance as another field is in the way and a shed is on the other side.

### Some of our Sources

Some of our inspiration is from a [high tunnel article](https://extension.illinois.edu/blogs/garden-scoop/2017-11-29-high-tunnels) by Ryan Pankau.

Some of our background intuition comes from the [Engineering Principles Impacting High-tunnel Environments](https://sustainable-farming.rutgers.edu/wp-content/uploads/2014/09/High-Tunnel-Engineering-HortTech-Giacomelli-2009.pdf) by Gene A. Giacomelli.

## 2/15/2022 ECE Team meeting with TA

Discussed some of the issues of our project and the possibility if this project does not work out.

* we would need to define the criteria for these conditions and this needs to be decided ASAP.

* The other adivse so far is to choose something and stick with it.

### Plans moving forward from here

1. We need to size our components and pick out components that work best for our situation.

2. We would like our project to work regardless if we can work with the ABE team or not. In other words, we want it to work whether or not we will be moving this greenhouse

3. Trying to get into contact with Matt to scope out the site

4. Questions that we need to ask ABE prof and team

### Specfic things that we need to look at a little bit more

* pick out a winch

* hall effect sensor

* pick out a magnet for the hall effect sensor

* pick out components for the system

## 2/19/2022 Team Meeting

First we meet with the ABE team to present and discuss some of our concerns as an ECE for the complexity of the project.

If the goals of the project are:

1. Price
2. Number of people required

Then, I do not see how this would be a viable ECE project since it is possible to buy a hand or electric winch that can be directly mounted to the ground by a post to move the high tunnel.

The ABE will have a meeting with Professor Grift on Monday about useablity.

The response from the ABE team was that they would prefer to get advise from the Professor Grift about the propects of the project.

### ECE Meeting

We wanted to raise our concerns for the project the project so we decided to email Professor Ann Witmer about our situation. From our suggestion with our TA, we started brainstorming ideas if we need to change projects. The main two project ideas that we came up with that is separate from the ABE team is the Mood Breathalyzer Project and the Whistleblower project.

#### Mood Breathalyzer Project

The goal of this project is to synchronize a person's breathing with a reflection in mood of their environment. The purpose of this project would be to help people mediate in a calm atmosphere. There are multiple ways that this could be accomplished but the main way is to break the project in the input sensors and the output room settings. For the input sensors, we discuss the possibility of using a tempature sensor to measure the breathing or a piezoelectric sensor. For the output of the system, we discussed having a change in tempo of music, an LED matrix, or an RGB LEDs.

#### Whistleblower

The title of the this project is really misleading since the real project would be a reverse version of the apple air tag. Using a UWB IC to pinpoint location, we could help people find a person who is in need especially in an unfamiliar setting. The project would include ICs for UWB, power conversion, amplification, and a microprocessor.

#### Other Ideas Not Worth Discussing

Biker's jacket with signaling system for cycling.

## 2/20/2022 ECE Team Meeting

The goal of this meeting is to finalize the other project more to present to the TA Evan.

We also got an email reply from Professor Ann Witmer that was pretty direct about our situation. We were all a little bit taken aback by it.

## 2/21/2022

### ABE Meeting with Dr. Grift

The Goal for this meeting was to get some clarity of the ECE team role in the project if the main objective of just moving the high tunnel does not fit the ECE Senior Design Criteria.

### Meeting Notes

First we introduced ourselves to Dr. Grift and Daniela and Carl started presenting on some of the issues and ideas that we had to solve the problem.

So far Carl has been the only person out of the 6 students that were able to make it out to the farm.

Dr. Grift recalls that a project has been done before on the farm. It also had to do with automation.

* I believe that is was to automate the ventalation system in the high tunnel.

#### Key Factors in Moving the High Tunnel

* Dr. Grift mentions that with a large structure and these small wheels that there maybe binding between the wheels and the rail especially since the high tunnel just sits over the winter.

* From the ABE Team, Matt mentions that they use half a day to lubricate the wheels and rails before moving the structure.

* One of Dr. Grift's questions is that if it is possible to replace all of the wheels and bearing on the high tunnel. This would improvement the amount of resistance in the system which would make it much easier to move.

* The ABE Team mentions that this might be too expensive and not cost effective.

* Dr. Grift mentions a few more observations about the system:
    1. The track looks temporary.
    2. 12V battery like a car battery would be needed to power a winch.
    3. The electrical challenge is not very big.

    4. Compares it in complexity to a garage door opener. A controller is cheap and relays are also cheap.
    5. The winch is the most expensive component which could be $150.
    6. The system needs the left and right side to move at the same speed otherwise it would bind up.
    7. Harbor Freight sells the components necessary to build this system.

* Can Torque me measured from each motor for two parallel winchs to pull the system at the same time?

    Dr. Grift answers:
    1. Could be possible.
    2. Two winches will never be in sync.
    3. A 12000 lb winch should be able to pull the structure.

    4. Feedback is creating a problem that is not there.
    5. Winches come with a remote control already.

* Dr. Grift Suggestion for the project to meet ECE Senior Design
    1. Make an app for the student farm
    2. Automate everything

* Dr. Grift's idea for the app is to bring together the controls for multiple parts of the farm like moving the high tunnels, irrigation systems, high tunnel ventilation and any other systems that the farm might add in the future.

* Dr. Grift mentions Steve Jobs and the idea that sometimes the customer does not always know what they want since they do not know what is possible.

* Dr. Grift mentions the Internet of Things type of project even though he does not like the name Internet of Things

* Dr. Grifts suggestions for things to measure.
    1. PIR sensor
    2. Sunshine
    3. Humidity
    4. Temperature
    5. Motor control sensors

* With this information, maybe make a system that can recommend when to move the high tunnel.

### Meeting with Evan

The goal of this meeting with our TA Evan is get more direct of our project ideas and to have more clarity on the direction that we will be heading for the rest of the semester.

The main take away from the meeting with Evan is that there is still going to be good project at this farm.

#### Points Brought up by Evan

1. Evan is not sold on idea that the tunnels need to be moved.

2. A single winch is too simple for 3 person design problem

3. Two winch system even if it is not necessary is good for control theory stuff.

4. Possibility to have an encoder on shaft and wireless commication aspect with two winches.

5. We need to modify the winch to communicate.

6. The reason a single winch would not work is because the system would only need to hold a relay until it pass some threshold.

#### Evan's Takeaways from the Meeting

1. Evan would like to talk to Dr. Grift again especially before next Monday

2. Changing projects away from the SSF is out of the question

3. Continue on with the project that is more of an execerise than a practical solution.

4. Evan will email Professor Grift about the meeting.

One of the issues that we are running into is that we are trying to more the project forward with the class deadlines

If we do schedule meeting try to CC Evan in case he is available to make it.

## 2/22/2022 ECE Team meeting

### Meeting with David Null, Evan Widloski, Professor Arne Fliflet

The goal of this meeting is to check in on our project to ensure that we understand the mechanical part of the project and to make sure that everyone is has some understanding of where we are heading.

Professor Fliflet has some concerns and pointers for our project:

* Pulling with one cable versus two cables with a beam across the width.

* Instead of measuring the amount of tension on the line, we can the motor current.
  * My interpretation is that there will be a slow build of tension in the line as more force is applied on the tunnel which will be reflected in the motor current. Once the tunnel overcomes friction and starts to move, there will be a sudden change in tension and motor current. These changes can be detected by the motor current sensor for the microcontroller. In addition, if there is any problems with the line as if it breaks, there will also be a change in motor current which the microcontroller should detect and stop.

* Would the structure be able to stop on its own by friction?

* The structure is not strong on its own and so aligment maybe an issue.

* The winch could have a warning system to tell the operator when to stop.

The reach goals of the project would include other tasks around the farm to automate. Another suggestion was if the motor could move the tunnel a certain distance.

### Going to the Farm

Professor Fliflet drove the team out to the student Sustainable farms where we met up with the ABE team. There we had a chance to look at the structure of the tunnel and take a few pictures of the contruction of the farm.

The space around the farm is not a lot but there definitely is room for a tractor on one end.

The goal is to move this thing at least once.

## 2/27/2022 ECE team Meeting For Design Review

Today the goal is to finish the design document.

ALthough the design document was not completed today there was some significant progress in explaining and writing out some of the details in the document like the various verification tables.

## 3/1/2022 Meeting with the TA

This is the weekly meeting with our TA Evan. The goal of this meeting is to check up on our progress and discuss some of the issues that our team is working through. Evan wants to make sure that there is a concrete project that is reasonable.

### Issues in our Current Implementation

* The Ultrasonic sensor is planned to be mounted outside of the main pcb which will be connected via a wire. If the user will be setting up this wire everytime that the tunnel needs to be moved, then the change of the position could be problematic for calculating the stopping distance to the end of the field.

* Our design does not identify how the system will reel out the winch to set up the system. As a team, we are not familiar with winch operation, so we have planned a two way switch for our PCB in case the user needs to reverse the direction of the winch. The goal is that we will have an accurate idea of this system once we are able to demonstrate the system with a winch.

* One critique to our system is that we originally planned for some sort of speed control with a feedback system. Since this system is very slow and winches are not made for feedback control, there is no need for speed control in this system.

* If the system is anchored to a truck then there is less mechanical stuff to build.

* Maybe there is one person that stands inside the tunnel with a remote control to monitor the tunnel as it moves.

* Some winches look like there are multiple speeds, how does that work?

* A safety stop would be nice to implement so that the winch does not continue to pull the tunnel beyond the end of the field.

* There is still concern that the project can be done with off the shelf components that would have better quality that our solution.

### What We Should Do

* Draw a break down of what is happening with the system. The idea is to show how we are modifying the system.

* Look inside the winch and the different circuits that are involved with it.

* Make changes to the design document. The block diagram is too hard to read. There needs to be thicker lines and bigger text.

* The visual aid needs to be a picture of the system and not a block diagram. In other words, we need a better physical diagram.

* need a new high level requirement for the battery charging and discharging. The focus needs to be on something that the system needs in order to be sucessful.

* The battery is not a good requirement.

* Error on the side of having less subsystems.

* Check if there is a third party system that would go well with our system.

* Put some more detail in the document.

## 3/3/2022 Meeting with Matt

The goal of this meeting is to update Matt on the progress of our project and discuss how we plan to attach the winch system to the ground.

### Mounting Options at the Farm

* There is accesss to a tractor or a truck with a trailer hitch. There is a tongue on the tractor.

* There is an option for the system to be mounted to stakes in the ground like ground screws.

* Matt is willing to put in a permanent piece of cement or struture in the ground.

### What Would We Need to Build

* Fabric something that would attach to the trailer hitch.

### Additional notes

* The plan is for a bar to go a cross the width of the tunnel.

* The winch is comming at the end of the month.

* Matt sent us pictures of the tractor attachment.

## 3/7/2022 Meeting with Dr. Grift and ABE Team

The goal of this meeting is to get another opinion on the bar system that the ABE team proposed.

### Discussed Topics

* How feasible is having a bar across the width of the tunnel with a pulley at each end?

* Would it be better to have a cable going all the way around to the other side of the tunnel or repeating the same structure on both sides.

* With a mounting point on a truck or a tractor, a trussel or triangular shape object can be made to center the pulling force from a single location. The concern is the high cost of very long cables.

### Electrical Design Discussion

How do we plan to control the tunnel?

* For current measurement, a shunt resistor can be used to measure the difference in voltage across a very small resistance. This way, high current can be measured without needed extra components.

* For turning the switch on and off, we can use a relay. There are many types of relays available, so we would need to look at the different specifications for our specific situation.

* The winch already has a solenoid box which is just a bunch of mechanical relays. The positive about this is that this keeps the circuit galvanically separated.

* One system that can be used to control the winch is an H-bridge. Depending on the switch layout configuration, the DC motor will have current going in either direction which will allow the motor to spin in both directions. May need to look into double pole double throw switches.

### Additional Notes

* The parts are coming in after break.

* The goal for monitoring the current would be to investigate whether or not there is a break in the cable for physical safety.

* The remote probably has more than just two wires to control the direction of the motor.

* May need to check if the change in the high amount of current can cause bouncing in the system. One way to reduce this system is to use a flyby diode for transistor switching

## 3/8/2022 Meeting with TA

The goal of this meeting is to give an update to the project for our TA.

Currently we have not quite finished the microcontroller circuit.

We need to meet more in person and define actionable items for everyone to work on until we meet each other next.

## 3/10/2022 ECE Meeting

The goal of this meeting is to define actionable goals for everyone in lab so that we can make good progress towards finishing this project.

### Updates from the Team

* We missed PCB and parts deadline with Evan.

* We need to tell machine shop that no ECE mechanical components for now. Hope that ABE tractor lab has enough components to mount our design to.

* Update design document as much as possible to reflect the current situation,

* Redesign our PCB to add both components and a bigger enclosesure. I will redesign PCB.

* Order Parts for the PCB design.

* Matt wants reliability.

### Notes on our team

Most of the work only gets done durring meeting

#### Jonathan

I will redo the PCB design with the microcontroller and power board.
Change the relate design document.

Email team over weekend for what time works best with everyone else.

#### Nisha

Nisha will write the logic code for the microcontroller

* a logic diagram for the design document
* code for the LCD

#### Youssef

Make a circuit diagram of the relay switches for the design document, so that other people who are reading the design document will understand what is going on.
This diagram will have the original circuit and how we are modifying the circuit for our project.

## 3/16/2022

Quick check up on the progress of the work during spring break.

### PCB Update

A new PCB schematic is in progress. It has not been order yet, but it is close to being finished. The design document will be need to be updated to match changes in the project.

### Other Updates

* The microcontroller code is in progress and the circuit schematic for the winch will be updated on the document.

* Youssef may be out of town for a few days for personal reasons so the exact days where everyone is available is not exactly known.

* As a team, we have order a few parts for the project which should arrive when we get back to campus. Then, we will be able to start testing our design.

## 3/22/2022 Meeting with the ECE Team

The goal of this meeting is to update each other on the status of our project.

Youssef will be back on campus soon, but he will be out of town on an upcomming Monday for personal reasons.

Nisha is currently programming the microcontroller and coming up with the state diagram.

The parts that are order are still in the process of shipping. The ABE has not ordered the winch. The ECE team may need a small winch to work on the design for our own testing.

### Meeting with the TA, Evan

Evan sent feedback on design document on some things that we should change.

#### Notes from Evan

* One person should focus on the design document while the team is waiting for parts.

* If we plan on using a truck or a tractor as an anchor point for the winch, then it has a 12V battery that we could use.

* The high amount of sustained current draw may drain the battery even with the alternator running. We may need to run some calculations on the feasibility of this.

## 3/23/2022 ECE Meeting with the Machine Shop

The goal of this meeting is to clarify and define some of the mechanical components to the design with Daniela from the ABE team and our TA Evan.

The plan for the tunnel that the ABE team will build a structure that will be positioned at the end of the tunnel to pull the winch from two seperate mounting points on each side of the tunnel.

One of the considerations is the angle of the pulling forces with the structure. Since we would not be pulling at 90 degree angles, there may be some lateral stress on the structure.

Another issue is that the cost of wire maybe far too expensive for the current project as it is currently planned since it involves pulling both directions from the same end of the tunnel. The ABE team may not have to funding for the full extent of the project.

We also have a polycase, that we plan to use to house the components in for the PCB.

## 3/26/2022 ECE Work Day

In this meeting, we were working on building and running the preliminary tests on our system. The goal was to get a working PCB with a microcontroller that can be programmed to communicate with the other sensors. Jonathan is working on putting the PCB board together, Youssef is working to testing some of the circuitry, and Nisha is working to programming the microcontroller.

### PCB Tests

With the Digital multimeter set to continuity or resistance, check that the ground pins are all connected and each trace has the proper connection on the PCB. In continuity mode, there is an audible beep for a good connection. In resistance mode, the resistance reading is relatively low less than 10 Ohms.

With the Digital multimeter set to continuity or resistance, check that pins that are not supposed to be connected are not connected. For example, one lead is connected to the ground plane while the other lead is connected to the power trace. The Digital multimeter should indicate an open loop.

After soldering on all of the components, I checked tested the continuity on the board once again to ensure that the ground, 5V, and 12V traces are separate.

From the first initial tests, the board without any other components draws 6mA of current with a 12V input. With the other components connected to the PCB without the current sensor and the ultrasonic sensor and the voltage supply set to 12V, it draws 11mA. Adding one LED to indicate the power going to the board increases the current to 12mA. The voltage regulation looks good and is very stable around 5.1V or 5.2V.

However, the switching circuit with the solid state relay is not working as expected. First we tested the circuit on the breadboard. We need a certain amount of current to allow the solid state relay to open or close. In order to resolve this issue, we diecided to switch out the resistance and the transistor until we got the amount of current that we expected.

The transistor number is: 59013 H. 331

Other than changing the gain of the transitor and solid state relay, the direction of current in the relay was opposite of the datasheet, so we just plan to switch the direction of the pins when connecting to the load.

## 3/28/2022 ECE Meeting

The goal of this meeting is to continue progressing on getting the microcontroller programed through a conbination of the SPI programmer and using the arduino board to burn the bootloader.

We were able to burn the bootloader and figured out that we may need to use a crystal oscillator as well.

One of the errors that we were getting was an identification error. Essentially, the program was expecting on ID for the microcontroller that we were using but read a different ID value. So, we tried modifying the one of the arduino files to change it to the same ID as the microcontroller. This allowed the bootloader to work, but we had truoble programming the microcontroller to blink an LED.

## 3/29/2022 ECE Meeting with Evan

This is our weekly meeting with our TA Evan. Outside of updating Evan on our progress, we wanted to ask Evan if we could get some help in programming our microcontroller regarding the ID problem.

Evan was able to make the microcontroller blink an LED light using the spi programmer. The PCB also fits nicely in the polycase and the holes for mounting the PCB in the case also line up.

## 3/31/2022 Meeting with ABE and MATT

The goal of this meeting is to continue discussing the plans for the tunnel in terms of project scope and to update the team on the plans for finishing this project.

### The New Plan

The original full scale plan was out of budget for the ABE team to purchase which was around 350 dollars for the winch plus any other supplies that team needed to build. For example, the amount of cable required in the plan is longer than twice the entire length of the field. Thus, the plan for the purposes of the ABE 469 and ECE 445 classes is to present a scale model of the project and leave Matt with a guide to make a full scale model.

If our system does not work, at the very least, the ABE team can leave Matt with a hand winch that will be able to pull the tunnel at the cost of speed.

### Notes from Matt

* There are some funds available from the farm to get the system working if needed.

* There are no current plans to move the tunnels this spring so there is some time to figure out and test the system at the farm.

* Matt has not currently ordered anything for the truck or the tractor in terms of mounting hardware.

* Matt works at the Fruit Research Building. I believe that it is located at 2711 south race street.

### Notes from ABE Team

* The ABE Team will make an exposed diagram of the parts that are going into the design.

* The wood lab is also available if needed.

* The settup is planned to have a 1/3 scale model of the winch with 2 extra hand winches on the side.

### Notes from ECE Team

* There is a 90 day return policy for winches at Harbor Freight

* The machine shop can make metal adapters for mounting the winch or other structures.

### Plans For the Future

After the demo for both ECE 445 and ABE 469, the teams will work on a manuel and system to make sure to leave Matt with something tangible after the project. Although this change came suddenly to Matt with out his knowledge, we will be able to modify the system afterwards in order to make it work for the full scale tunnel. We plan to have a full team meeting soon to get everyone together in a lab for another update.

## 4/5/2022 ECE Meeting with Evan

The goal of this meeting is to update everyone on our progress and continue working on the project.

### Hardware Updates

* Youssef purchased a winched from harbor frieght. This will allow a lot of testing done without needing to use the winch that the ABE team purchased.

* The PCB has some traces that are not correct, so we need to scratch out a few traces to make the board operational. Since many of our components are through-hole it is simple to resolder wires on the board for new connections.

* The LCD screen was able to display information with an arduino nano.

* The old PCB board has all of the new connections with the attached wires on the back side of the board.

### Next Steps

* We are using the winch to figure out the operation of the different solenoids and how to interface with the devices.

* For the code on the microcontroller, we are working on getting the program to display the proper information to the LCD screen.

### Sample Connections

PB5 SCK

PB4 MISO

PB3 MOSI

## 4/7/2022 Meeting with ECE team

Another work day to continue working on testing the circuits on and off the PCB board. First, we tested the LCD on a breadboard to see if we could communicate with it from any microcontroller, then test with our microcontroller. Once both of those tests pass, then we can test if we can communicate with the LCD board on the PCB.

The winch also came in today. The main objectives with the winch is to understand the operation and how to modify the circuit and mechanical connections for our project.

## 4/11/2022 Meeting with ECE Team

The goal of this meeting is to understand the winch better.

### Testing the Winch

* Can the winch unspool and respool with the given hardware? When pulling with the remote that came with the winch, nominal current of the winch is roughly 10 Amps and peaked at 22 Amps for a split second. The operation of the winch is how we expected.

* The relay box on the winch is where we plan to tap into the system. Instead of using our own relay to switch the supply to the winch, we can use a relay to replicate the hand remote that came with the winch, so that either switch can operate the winch. The only concern is to make sure not to try to use both systems at the same time.

## 4/12/2022 Meeting with Evan

The goal of this meeting is to update Evan on the progress of the project and to continue working on getting the system operational. From the physical design, we can have the machine shop drill holes in the polycase with cable grips for the connections outside of the polycase.

For cable connections to the battery or the winch, we can use cable crimps with on male and one female connector so that they can not be reversed. For testing, the cable crimps may not be necessary.

## 4/13/2022 Meeting with ECE team

The goal of this meeting is to continue testing of the sensors on the breadboard and on the PCB.

### Updates to Hardware

* The second switch is in the machine shop and they should have it mounted to the polycase by the end of the day.

* The the short wires for the L37S300S05M current sensor have been made for testing the current sensor.

* The Ultrasonic sensor is able to read properly and display the distance to the LCD screen.

* The new PCB is in the lab, but we may decide not to solder it because the old PCB works just as well. There are traces that we would need to modify on both PCBs. The main problems that we are working on are not related to the PCB. Rather, we are trying to get the different sensors to properly communicate with the microcontroller.

* One of the issues that we have is that the current sensor is not reading values as we expected. The change in the voltage output reading even when we are drawing around 10 amps with the winch changes the voltage on the current sensor barely by a 1mV if we could even detect it.

## 4/17/2022 ECE meeting

The goal of this meeting is to continue work at the lab bench and continue integrating the design with the PCB instead of the breadboard.

### Notes about the Progress

* Over the weekend on 4/15 and 4/16, the as system connected through the front panel switches and the tapping into the solenoid box work as expected.

* A new circuit diagram of the system is needed to explain the connection changes and how the system is modified from the original.

* A new current sensor that comes with its own display is being tested. It was able to read 5 amps and 10 amps from our winch properly.

### Issues in Lab Session

* The connection to our microcontroller is not working properly. The microcontroller is not able to read the correct value from the current sensor.

* Sometimes the LCD display does not properly output the correct information. It appears to be scrambled at times.

## 4/18/2022 ECE Meeting

The goal of this meeting is to continue work on the project and to see if we can resolve the issues that we came across in the last lab session.

### New programming logic

* In order to comply with the duty cycle operation of the winch from the manufacturer specifications, the microcontroller will cut the power to the winch accordingly.

* The ultrasonic sensor will measure the distance that the tunnel is close to the end of the of the field. When the tunnel reaches a predetermined distance, the microcontroller will stop powering the winch until it is reset.

### Issues during this lab session

* The current sensor is broken and the cable that goes to the LCD display has loose connections.

* There are some issues with our soldering connections.

## 4/19/2022 Work Day

The goal of this meeting is to continue work on our project and some integration testing with the ABE team.

### Work with ABE Team

* The ABE team got a discounted price on the 5000 lbs winch. However, we are using our winch for the test pulls.

* The 1/3 scale model structure is complete and is ready for testing.

#### Tractor Lab and ABE Testing

* The winch is mounted to the tail hitch on the back of the pickup truck with bolts, nuts, and washers.

* There is a cart with a concrete barrel and an anvil to use as a test weight for the high tunnel.

* The winch pulls directly on the cable attachment that is on the ABE force distribution structure.

#### Results of ABE Test

* The winch was able to pull the test weight using the front panel controls.

* The LCD screen and the current sensor was not able to be tested since the LCD cable was broken and the current sensor was broken.

* The ABE was able to take a video of the test run for their class.

* The ABE struture broken when we started pulling the structure. However, we almost broke our winch. The winch cable was a little bit loose when reeling in the cable, so the cable got pinched in the housing.

### Work at ECEB

* The machine shop was able to fix the pinched cable.

* One of the issues that we continually run into every now and then is that the LCD screen sometimes displays the correct infomation and sometimes does not display any relevant information.

* Other changes that we are making is rerouting the connections on the board to make sure that the analog signals are connected to the analog inputs on the microcontroller.

## 4/22/2022 ECE Full System Integration

The goal of this meeting is to integrate the full system on the PCB so that we can have a working model for the demonstration. We would like to integrate the new current sensor and the LCD cable to test the system on the winch in the polycase.

### Hardware Updates For Full System Integration

* The LCD and microcontroller had trouble reading the voltage and displaying to the LCD on the PCB. This issue was resolved in double checking our wiring for all of the LCD pins on the PCB.

  * enable to  PD1
  * current sensing to PC5
  * RS to PC4

## 4/23/2022 Continued Testing for Full ECE System Integration

The goal of this meeting this meeting to fix the issues that we encountered in the last meeting to make a working prototype for the demonstration. The next goal is to finish putting the case together to reduce the length of the long cables and make the system look polished.

### Connection to Microcontroller

After connecting the system together, there seems to be some variability in how consistent the system is to turn on. In other words, the power from the power converter is working properly as indicated by the front panel LED, however the LCD display does not always properly display the correct imformation. After some investigation, the problem seems to be a loose connection between the LCD and the micrcontroller. Since we are still testing different variations of code, we do not want to solder down the microcontroller to hopefully fix the issue. Even though there is a 6-pin programming port on the board, there is still the possiblity that it might fail on our only operational project board.

Our solution is that sometimes extra pressure on the microcontroller or moving it around slightly can reconnect the pins and allow the LCD to display the expected information.

### Current Sensor Calibration

Originally, the LCD screen displays the digital value of the current from the current sensor. However, we wanted to change this to allow for the actual analog value so that the user has a better idea of the current draw from the battery.

While running connecting the current sensor to the system we measured:
|Actual|Digital|
| --- | ----------- |
| 0 | 705 |
| 8.25 | 705 |
| 11.6 | 705 |

It turns out that the our pin connections were incorrect and we need to change them around. Our second test results:

|Actual|Digital|
| --- | ----------- |
| 0 | 528 |
| 8 | 560 |

These number make more sense since a reference voltage for the internal error amplifier might be $V_{dd}$/2 which correlates to zero current flowing through the current sensor. A output reading higher that the reference would indicate positive current and a output reading lower than the reference would indicate a negative current. With a 10-bit ADC the theoretical read would be 512 which is close to our measured value of 528.

### Front panel LEDs

One issue with the design of the case is that the LEDs are not bright enough to be visible. Thus, we decided to change out the red LEDs for blue LEDs that shine brighter.  

### Running Tests

A few of the tests that we would like to run before the demonstration:

|Higher current limit|Lower current limit|
| --- | ----------- |
| Winch in | Winch in |
| Winch out | Winch out |
| Winch Stop via timer | Winch Stop via timer |
| Winch Stop via Ultrasonic sensor | Winch Stop via Ultrasonic sensor |

These test cover the different senarios of the operation of the winch. One of the problems while performing these tests is that the code on the microcontroller needs to be changed to switch between the tests with the higher current limit and the lower current limit. Since we were having trouble with the microcontroller having a solid connection to the LCD, the final Demonstration will include the tests with a specific current limit instead of risking the system to stop functioning properly during the demonstration.

## 4/24/2022 Last day before Demonstration

The goal of this is to finalize the block diagram, high level requirements, and requirements for the demonstration. The other goal is to clean up the system to look as neat as possible.

### Cleaning up the appearance of the system

* In order to mount the poly case and the solenoid box to the truck mounting plate, we originally used some copper wire to tie it down. Even though this worked for holding the system together, we wanted to minimize the extra wires so we changed it out for some double sided tape. This did not hold the components together as well, but it was good enough to last for atleast a few days.

* During the trial and error phase of testing our device, we made the wires to each component fairly long. Although this was helpful for debugging and trying to isolate the issue from the rest of the system, it brought about an additional problem of trying to fit everything inside of the case. Thus, we shorted each of this wires to ensure that there would be enough space inside the case.

* Since the case is going to be used outdoors, we wanted to make sure that the case has some protection against the weather. Thus, on 4/14/2022 we purchased and ordered some cable grips to cover the holes on the sides of the case. However, these did not arrive before we shorted the wires to the printed circuit board. We even ordered the cable grips a second time on 4/19/2022, but these did not arrive until 2/25/2022.

## 4/25/2022 Demonstration Day

The goal of this meeting is to print out the tables for our demonstration and present our project. In the case that the system stops working, we want to be able to show the operation of the system with a video demonstration.

From our meeting with the ABE team on 4/19/2022, we have a video demonstration of the winch pulling a test load. In our own tests, we a few videos of operating the winch to test for certain conditions, but not all of the conditions at once. For example, we have a video of the winch reeling out, reeling in, and the ultrasonic sensor triggering. Many of the other videos did not turn out as intended because of the loose connections on the microcontroller.

## 4/28/2022 Mock presentation

In this meeting, we want to make adjustments to our final presentation to figure where certain slides should be positioned in the presentation and if we need to add extra components to the presentation. During the mock presentation, wee got back some of the following critiques:

* No complete sentences. The slides need to be readable and easily understandable by the audience. A big block of text can distract from the presentation.

* Put the picture first in the objectives. The picture can give a much better picture of the overall design and scale of the project to the view faster than trying to explain it to the audience.

* Introduce yourselves and state the part of the project that you worked on. This can help the audience know who to direct questions towards. It also helps the audience a little bit about each person.

* More even talking between group members. In our mock presentation, one person talked through the project more than the other people. It would be better if the presentation was split between the group members a little bit more evenly.

* Interact with the presentation. During our presentation, we mostly stood still by the sides of the projector screen. It would be more enjoyable and engaging for the audience if we were more excited about our presentation. The other additional note is that we should feel free to point to objects on our presentation or point out various characteristics.

* Put the subsystems and RV tables in the presentation and talk about the purpose. We originally did not have the RV tables and subsystems specifically describled in the mock presentation, so the TA advised us to include them.

* Use pictures over schematics if possible. The idea is that a picture can give the audience a better view of subsystem then a schematic.  

* Animated slides. Some small animated slides can add to the presentation by directing the attention of the audience to the part of the slide that the speaker is talking about instead of being overwhelmed by all of the information on the slide.

* Video demonstration. A video during the presentation of the operation of the system or a key test of the system can add to the visual aid of the presentation.

## 4/30/2022 Work on presentation and final report

This meeting was to work on the presentation and the final report for ECE 445. Our presentation is scheduled for Tuesday 5/3 and the report is on Wednesday 5/4. A few of the descriptions that we worked on were the power, front panel, sensors, and winch system.

## 5/1/2022 Remote work on presentation and final report

The goal of this meeting is to continue working through the presentation and the final report. We want to make sure that our tables fit on the page properly, so I spent some time trying to reformat the final paper. Another challenge was the bibliography. The style of the bibliography file took some time to understand and insert our own sources.

## 5/2/2022 Presentation meeting

The goal of this meeting is to finish the presentation. Since we decided that we work the best at the ECE 445 lab, we decided meet up to finish up the slides.

## 5/3/2022 Meeting for Presentation

The goal of this meeting is to finalize the pacing of the presentation and to delivery the presentation to our TA and professor.

## 5/4/2022 Meeting for Final Paper

The goal of this meeting is to finish the final paper. We added sections to the design and verification of the winch system. We also added pictures for the visual aid.

## 5/5/2022 Last meeting for Lab checkout

This is the last meeting to checkout of the lab and attend the awards ceremony with free pizza.
