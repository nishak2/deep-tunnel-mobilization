# Deep Tunnel Mobilization

Team Members:
- Nisha Kolagotla (nishak2)
- Jonathan Young (jy30)
- Youssef Elmokadem (youssef3)

# Problem
Describe the problem you want to solve and motivate the need.

Late in the season, most vegetables are done for the season except some cold hardy crops. A hoop house or deep tunnel can be used to extend growing periods. A hoop house or deep tunnels, equipt with UV resistant polyethylene plastic, can be used to extend growing periods by protecting them from direct sunlight. If deep tunnels are moved, one deep tunnel can be used as an incubator for more crops in a given season than if it is not moved. Currently 10-12 volunteers are needed to move these deep tunnels at the sustainable student farms which is an inconvenience.  Ideally it would only take 1-2 volunteers to move these deep tunnels so the farmers are not dependent on having a large group congregate to move the tunnels. This problem was pitched by Professor Ann Witmer during lecture.

# Solution
Describe your design at a high-level, how it solves the problem, and introduce the subsystems of your project.

Design, build, and test a moving deep tunnel. The motor drives a gear and gear rack system to move the deep tunnel along a plot of land. The tunnel will be put on wheels and a track by the ABE part of the team. A gear rack will be fixed to the side of the deep tunnel.  A gear driven by an electric motor will be placed in the rack moving the tunnel along the track. Data about the operation of the system will be collected so as to prevent damage to the deep tunnel if a malfunction with the system occurs by shutting off the system.

# Solution Components

## MCU

The MCU will be a SMT32 and the exact model will be determined later due to parts and availability. This microcontroller provides the computing power to tell when to activate the motors and other sensors. The main controller logic is written in C. 

## Motor controller 

The motor controller is a control circuit that takes the PWM signal from the microcontroller to drive the motor in either direction. This circuit is responsible for providing enough power to the motors.

## Battery System

Since this is an off-grid system, the battery is responsible for the energy storage while the battery management system is responsible for updating the microcontroller on the state of charge and keeping the battery from overheating, over discharging, overcharging, and cell balancing. The battery cells will be removable for recharging.

## Motor

At least two large motors will be needed to drive each side of the deep tunnel. These motors will work in tandem to produce parallel linear motion along the field. The exact motor type and power rating will be determined by the weight and size of the deep tunnel. 

## Sensors

A gyroscope sensor will determine how fast the wheels are turning on both tracks. This will determine if one side is moving faster than the other and correct the discrepancy. If one side of the tracks has debris on it or if one of the motors is not working correctly, the resulting asymmetry could be detrimental to the structure of the greenhouse. This sensor will detect the asymmetry.

Temperature sensor for the batteries. If the batteries overheat the MCU will send an error message and stop the motors from running.

Position sensor for the location of the deep tunnel in relation to the field. This sensor will let the MCU know if there is any space to move further along the rails or if the end of the field has been reached.

#Interface

On/off button
Switch for direction
Another button to execute the shift in the specified direction

# Criterion For Success

The deep tunnel is bi-directionally mobile along a track via an applied force parallel to the track in a way that will not compromise the integrity of the deep tunnel. 

We will make one tunnel mobile with a reach goal of making the three tunnels mobile. 

The tunnel will be made moveable requiring at most 2 operators of the system. 

The temperature sensor reports a fault to the MCU when the temperature is outside the operating range.

The position sensor reports the proximity to the end of the track and prevents the deep tunnel from moving beyond the end of the track. 
