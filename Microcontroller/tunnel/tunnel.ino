#include <LiquidCrystal.h>

//ultrasonic sensor
const int echoPin = PIN_PB7, trigPin = PIN_PD5;
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement

//lcd panel
const int rs = PIN_PC4, en = PIN_PD1, d4 = PIN_PC0, d5 = PIN_PC1, d6 = PIN_PC2, d7 = PIN_PC3, rw = PIN_PD0;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Other pins
const int LED1 = PIN_PD6, current = PIN_PC5, relay = PIN_PB0;

void setup() {
  //LCD
  pinMode(rw, OUTPUT);
  digitalWrite(rw, LOW);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  //other pins
  pinMode(current, INPUT);
  pinMode(relay, OUTPUT);
  digitalWrite(relay, HIGH);
  pinMode(LED1, OUTPUT);
  digitalWrite(LED1, LOW);
}

void loop() {
  //Ultrasonic sensor
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance in cm
  distance = duration * 0.034 / 2;

  //Print distance
  lcd.setCursor(0, 0);
  //reset lcd row 1
  for (int i = 0; i < 16; ++i)
  {
    lcd.write(' ');
  }
  lcd.setCursor(0, 0);
  lcd.print(distance); //prints distance in cm
  lcd.print(" cm");
  delay(10);
  
  //reset lcd row 2
  lcd.setCursor(0, 1);
  for (int i = 0; i < 16; ++i)
  {
    lcd.write(' ');
  }

  //current sensor
  lcd.setCursor(0, 1);
  float current1 = 8*(analogRead(current)-528)/31;
  lcd.print(int(current1));

  //Relay
  lcd.setCursor(8,0);
  //if off, stay off until system reset
  if(off ==1)
  {
    digitalWrite(relay, LOW);
    lcd.print("OFF");
    delay(20);
    off = 1;
    digitalWrite(LED1, HIGH);
  }
  else if(analogRead(current) > 700)
  {
    digitalWrite(relay, LOW);
    lcd.print("OFF");
    delay(20);
    off = 1;
    digitalWrite(LED1, HIGH);
  }
  else if(distance<20)
  {
    digitalWrite(relay, LOW);
    lcd.print("OFF");
    delay(20);
    off = 1;
    digitalWrite(LED1, HIGH);
  }
  else if((millis()% 90000) >45000)
  {
    digitalWrite(relay, LOW);
    lcd.print("OFF");
    delay(20);
  }
  else
  {
    digitalWrite(relay, HIGH);
    lcd.print("ON");
    delay(20);
  }

  //Clock
  lcd.setCursor(8, 1);
  if(millis()<60000)
  {
    lcd.print("00:");
    if(millis()<10000)
    {
      lcd.print("0");
    }
    lcd.print(millis()/1000);
  }
  else if(millis()<3600000)
  {
    if(millis()<600000)
    {
      lcd.print("0");
    }
    lcd.print(millis()/60000);
    lcd.print(":");
    if((millis()%60000)/1000 < 10)
    {
      lcd.print("0");
    }
    lcd.print((millis()%60000)/1000);
  }
  else if(millis()<86400000)
  {
    if(millis()<36000000)
    {
      lcd.print("0");
    }
    lcd.print(millis()/3600000);
    lcd.print(":");
    if(millis()<600000)
    {
      lcd.print("0");
    }
    lcd.print(millis()/60000);
  }
  else
  {
    lcd.print(">1day");
  }

}
