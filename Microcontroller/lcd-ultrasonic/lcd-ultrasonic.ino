// include the library code:
#include <LiquidCrystal.h>

#define echoPin PIN_PD1
#define trigPin PIN_PD2

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = PIN_PC4, en = PIN_PC5, d4 = PIN_PC0, d5 = PIN_PC1, d6 = PIN_PC2, d7 = PIN_PC3;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
const int PowerCutOff = PIN_PB0, Vout = PIN_PB1, Vref = PIN_PC5, PanelLED = PIN_PD1;
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement
int distanceInch;

void setup() {
  pinMode(PIN_PD0, OUTPUT);
  digitalWrite(PIN_PD0, LOW);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  //lcd.print("hello, world!");
}

void loop() {
  //Ultrasonic
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  
  lcd.setCursor(0, 0);
  lcd.print("Distance: ");
  lcd.print(distance);
  lcd.print(" cm");
  delay(10);
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  //lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  //lcd.print(millis() / 1000);

  lcd.setCursor(0, 1);
  lcd.print("Distance: ");
  distanceInch = distance;
  lcd.print(distanceInch);
  lcd.print(" inch");
  delay(10);
}
